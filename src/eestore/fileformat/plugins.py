from io import StringIO
import csv

from django.utils.timezone import now as utcnow

import requests

from eestore.plugins import PluginMount, BaseAPIPlugin
from eestore.utils import generate_pid

from .models import FileFormat

__all__ = [
    'BaseFileFormatPlugin',
    'LocalFileFormatPlugin',
]


class BaseFileFormatPlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all FileFormat plugins

    Attributes:

    plugintype          'fileformat'
    model               FileFormat
    """
    plugintype = 'fileformat'
    model = FileFormat


class PRONOMFileFormatPlugin(BaseFileFormatPlugin):
    pluginname = 'pronom'
    browseable = True
    source_endpoint = None
    url_format = None
    readonly = True
    auth_needed = False
    queryable_by_date = False

    def get_data_for_single_repo(self, obj):
        data = {}
        data['source'] = self.pluginname
        data['remote_id'] = obj['PUID']
        data['remote_pid'] = obj['PUID']
        data['pid'] = generate_pid(self.plugintype, data['source'], data['remote_id'])
        data['extensions'] = tuple(filter(None, obj['Extension'].split(',')))
        data['version'] = obj['Format Version']
        data['name'] = obj['Format Name']
        return data

    def get_all_data(self, after=None):
        csv_url = 'http://www.nationalarchives.gov.uk/PRONOM/Format/proFormatListAction.aspx'
        params = {
            'strAction': 'Save As CSV',
            'strLastQueryStatus': 'format',
            'strLastQueryExtension': '%%',
            'strLastQueryFileExtension': '%%',
            'strOrderBy': 'order_extension',
        }
        r = requests.post(csv_url, data=params)
        CSV = StringIO(r.text)
        csvrows = csv.DictReader(CSV)
        data = []
        for row in csvrows:
            data.append(self.get_data_for_single_repo(row))
        return data
