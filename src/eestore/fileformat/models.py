from django.db import models
from django.contrib.postgres.fields import ArrayField

from ..utils.models import AbstractRepo


class FileFormat(AbstractRepo):
    version = models.CharField(max_length=255, blank=True)
    extensions = ArrayField(
        models.CharField(max_length=32),
        blank=True,
        null=True,
    )

    class Meta:
        db_table = 'eestore_fileformat'
