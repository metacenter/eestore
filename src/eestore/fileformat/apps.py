from django.apps import AppConfig


class EEStoreFileFormatConfig(AppConfig):
    name = 'eestore.fileformat'
    label = 'eestore_fileformat'
    verbose_name = 'File Format'
