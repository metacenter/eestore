from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import FileFormat


class FileFormatSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileFormat
        fields = STANDARD_FIELDS + ['version', 'extensions']


class FileFormatViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = FileFormat.objects.all()
    serializer_class = FileFormatSerializer
    search_fields = FilterMixin.search_fields + ('version', 'extensions',)
