from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EEStoreAuthConfig(AppConfig):
    name = 'eestore.auth'
    verbose_name = _("EEStore Authentication and Authorization")
    label = 'eestore_auth'
