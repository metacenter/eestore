from django.db import models

from ..utils.models import AbstractRepo


class ProjectRepo(AbstractRepo):
    description = models.TextField(blank=-True)
