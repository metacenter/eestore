from eestore.plugins import PluginMount, BaseAPIPlugin
from eestore.plugins.utils import trunc_name
from eestore.eapis.cristin import CRISTINV2Mixin
from eestore.utils import generate_pid

from .models import ProjectRepo

__all__ = [
    'BaseProjectRepoPlugin',
    'LocalProjectRepoPlugin',
    'CRISTINProjectRepoPlugin',
]


class BaseProjectRepoPlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all ProjectRepo plugins

    Attributes:

    plugintype          'projectrepo'
    model               ProjectRepo
    """
    plugintype = 'projectrepo'
    model = ProjectRepo


class CRISTINProjectRepoPlugin(CRISTINV2Mixin, BaseProjectRepoPlugin):
    source_endpoint = 'http://api.cristin.no/v2/projects'
    url_format = 'https://app.cristin.no/projects/show.jsf?id={}'

    def get_data_for_single_repo(self, jsonblob):
        assert type(jsonblob) in (list, dict), 'Wrong format for input'
        data = {}
        data['source'] = self.pluginname
        data['remote_id'] = jsonblob['cristin_project_id']
        data['pid'] = generate_pid(self.plugintype, data['source'], data['remote_id'])
        name = self._select_language_object(jsonblob, 'title')
        data['name'] = trunc_name(name)
        url = self.get_url(data['pid'])
        if url:
            data['uri'] = url
        return data
