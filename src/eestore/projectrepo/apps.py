from django.apps import AppConfig


class EEStoreProjectrepoConfig(AppConfig):
    name = 'eestore.projectrepo'
    label = 'eestore_projectrepo'
    verbose_name = 'Project Repository'
