from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import ProjectRepo


class ProjectRepoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectRepo
        fields = STANDARD_FIELDS + ['description']


class ProjectRepoViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = ProjectRepo.objects.all()
    serializer_class = ProjectRepoSerializer
    search_fields = FilterMixin.search_fields + ('description',)
