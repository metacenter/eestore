from unittest import TestCase

from django.test import TestCase as DjangoTestCase

from .plugins import BaseDataRepoPlugin


class BaseDataRepoPluginTest(TestCase):

    def test_to_json(self):
        class TestClass:
            foo = 1

        json = BaseDataRepoPlugin().to_json(TestClass())
        self.assertIn('''"type": "datarepo"''', json)
