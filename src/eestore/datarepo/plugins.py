from os.path import join as pathjoin
import xml.etree.cElementTree as etree

from django.utils.timezone import now as utcnow

import requests

from eestore.plugins import PluginMount, BaseAPIPlugin
from eestore.plugins.utils import dumpjson
from eestore.utils import generate_pid


from .models import DataRepo

__all__ = [
    'BaseDataRepoPlugin',
    'LocalDataRepoPlugin',
    'Re3DataRepoPlugin',
]


class BaseDataRepoPlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all DataRepo plugins

    Attributes:

    plugintype          'datarepo'
    model               DataRepo
    """
    plugintype = 'datarepo'
    model = DataRepo


class Re3DataRepoPlugin(BaseDataRepoPlugin):
    pluginname = 're3data'
    source_endpoint = 'http://www.re3data.org/api/'
    readonly = True
    auth_needed = True
    browseable = True
    queryable_by_date = False
    url_format = 'https://www.re3data.org/repository/{}'

    def get_data_for_single_repo(self, obj):
        data = {}
        data['source'] = self.pluginname
        pid = obj.find('id').text
        data['remote_id'] = pid
        data['pid'] = generate_pid(self.plugintype, data['source'], pid)
        name = obj.find('name')
        data['name'] = name.text
        url = self.get_url(pid)
        if url:
            data['uri'] = url
        return data

    def get_all_data(self, after=None):
        r = requests.get(pathjoin(self.source_endpoint, 'v1/repositories'))
        root = etree.XML(r.content)
        data = []
        for repo in root.findall('repository'):
            data.append(self.get_data_for_single_repo(repo))
        return data

    def query(self, query):
        assert query, '"query" cannot be empty'
        self.fill()
        objs = self.get_queryset().values()
        return dumpjson(data)

    def lookup(self, pid):
        r = requests.get(pathjoin(self.source_endpoint, 'beta/repository', pid))
        root = etree.XML(r.content)
