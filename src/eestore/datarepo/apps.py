from django.apps import AppConfig


class EEStoreDatarepoConfig(AppConfig):
    name = 'eestore.datarepo'
    label = 'eestore_datarepo'
    verbose_name = 'Data Repository'
