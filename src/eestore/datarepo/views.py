from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import DataRepo


class DataRepoSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataRepo
        fields = STANDARD_FIELDS


class DataRepoViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = DataRepo.objects.all()
    serializer_class = DataRepoSerializer
