from pathlib import Path

from bs4 import BeautifulSoup
import requests

from eestore.plugins import PluginMount
from eestore.plugins import BaseAPIPlugin
from eestore.utils import generate_pid

from .models import Taxonomy

__all__ = [
    'BaseTaxonomyPlugin',
    'LocalTaxonomyPlugin',
    'TaxonomyWarehousePlugin',
]


class BaseTaxonomyPlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all Taxonomy plugins

    Attributes:

    plugintype          'taxonomy'
    model               Taxonomy
    """
    plugintype = 'taxonomy'
    model = Taxonomy


class TaxonomyWarehousePlugin(BaseTaxonomyPlugin):
    pluginname = 'taxonomywarehouse'
    source_endpoint = 'http://taxonomywarehouse.com/headword_list_new.aspx?vobject=10076&vbegrange=a&vendrange=z'
    browseable = False
    readonly = True
    auth_needed = False
    queryable_by_date = False

    def get_data_for_single_repo(self, obj):
        # <a href="<uri>"><name></a>
        data = {}
        data['source'] = self.pluginname
        data['uri'] = obj.get('href')
        data['name'] = obj.text
        data['remote_id'] = data['uri'].split('vunid=')[-1]
        pid = generate_pid(self.plugintype, self.pluginname, data['remote_id'])
        data['pid'] = pid
        return data

    def get_all_data(self, after=None, content=None):
        if not content:
            r = requests.get(self.source_endpoint)
            soup = BeautifulSoup(r.content, 'html.parser')
            content = soup.find(id='MainBody_divSearchResults')
        data = []
        for link in content.find_all('a'):
            data.append(self.get_data_for_single_repo(link))
        return data
