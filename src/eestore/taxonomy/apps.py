from django.apps import AppConfig


class EEStoreTaxonomyConfig(AppConfig):
    name = 'eestore.taxonomy'
    label = 'eestore_taxonomy'
