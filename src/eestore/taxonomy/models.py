from django.db import models

from ..utils.models import AbstractRepo


class Taxonomy(AbstractRepo):
    pass

    class Meta:
        verbose_name_plural = 'taxonomies'
