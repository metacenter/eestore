from eestore.plugins import PluginMount, BaseAPIPlugin
from eestore.plugins.utils import trunc_name
from eestore.eapis.cristin import CRISTINV2Mixin
from eestore.utils import generate_pid

from .models import OrganizationRepo

__all__ = [
    'BaseOrganizationRepoPlugin',
    'LocalOrganizationRepoPlugin',
    'CRISTINOrganizationRepoPlugin',
]


class BaseOrganizationRepoPlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all OrganizationRepo plugins

    Attributes:

    plugintype          'organizationrepo'
    model               OrganizationRepo
    """
    plugintype = 'organizationrepo'
    model = OrganizationRepo


class CRISTINOrganizationRepoPlugin(CRISTINV2Mixin, BaseOrganizationRepoPlugin):
    source_endpoint = 'http://api.cristin.no/v2/institutions'

    def get_data_for_single_repo(self, jsonblob):
        assert type(jsonblob) in (list, dict), 'Wrong format for input'
        data = {}
        data['source'] = self.pluginname
        data['remote_id'] = jsonblob['cristin_institution_id']
        data['pid'] = generate_pid(self.plugintype, data['source'], data['remote_id'])
        data['acronym'] = jsonblob.get('acronym', '')
        name = self._select_language_object(jsonblob, 'institution_name')
        data['name'] = trunc_name(name)
        return data
