from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import OrganizationRepo


class OrganizationRepoSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrganizationRepo
        fields = STANDARD_FIELDS + ['acronym']


class OrganizationRepoViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = OrganizationRepo.objects.all()
    serializer_class = OrganizationRepoSerializer
    search_fields = FilterMixin.search_fields + ('acronym',)
