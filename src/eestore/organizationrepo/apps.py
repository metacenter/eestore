from django.apps import AppConfig


class EEStoreOrganizationrepoConfig(AppConfig):
    name = 'eestore.organizationrepo'
    label = 'eestore_organizationrepo'
    verbose_name = 'Organization Repository'
