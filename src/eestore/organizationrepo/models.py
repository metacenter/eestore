from django.db import models

from ..utils.models import AbstractRepo


class OrganizationRepo(AbstractRepo):
    acronym = models.CharField(max_length=16, blank=True)
