from rest_framework import status as STATUS
from rest_framework.response import Response


__all__ = [
    'AcceptedResponse',
    'SeeOtherResponse',
    'Http404Response',
]


class AcceptedResponse(Response):

    def __init__(self, location, data=None, status=None, headers=None, **kwargs):
        status = STATUS.HTTP_202_ACCEPTED
        if headers is None:
            headers = {}
        headers['Location'] = location
        return super().__init__(data, status=status, headers=headers, **kwargs)


class SeeOtherResponse(Response):

    def __init__(self, see_other, data=None, status=None, headers=None, **kwargs):
        status = STATUS.HTTP_303_SEE_OTHER
        if headers is None:
            headers = {}
        headers['Location'] = see_other
        return super().__init__(data, status=status, headers=headers, **kwargs)


class Http404Response(Response):

    def __init__(self, data=None, status=None, headers=None, **kwargs):
        status = STATUS.HTTP_404_NOT_FOUND
        if headers is None:
            headers = {}
        return super().__init__(data, status=status, headers=headers, **kwargs)
