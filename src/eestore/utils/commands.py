from django.core.management.base import BaseCommand

import eestore


__all__ = [
    'EEStoreCommandMixin',
    'EEStoreBaseCommand',
]


class EEStoreCommandMixin:

    def get_version(self):
        django_version = super().get_version()
        msg = '{} (Django {})'
        return msg.format(eestore.__version__, django_version)


class EEStoreBaseCommand(EEStoreCommandMixin, BaseCommand):
    pass
