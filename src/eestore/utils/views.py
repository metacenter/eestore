from collections import OrderedDict

from django.core.exceptions import ValidationError as DjangoValidationError
from django.template import loader

from rest_framework.filters import BaseFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.filters import OrderingFilter
from rest_framework.serializers import ValidationError as DRFValidationError


__all__ = [
    'STANDARD_FIELDS',
    'EEStoreRepoFilterBackend',
    'FilterMixin',
]

STANDARD_FIELDS = ['id', 'source', 'pid', 'remote_id', 'remote_pid', 'name',
                   'uri', 'last_fetched']


class EEStoreRepoFilterBackend(BaseFilterBackend):
    template = 'eestore/utils/repofilter.html'
    filter_fields = {
        'source': [''],
        'last_fetched': ['', 'lt', 'gt', 'lte', 'gte'],
    }
    query_label = {
        '': 'is identical to',
        'lte': 'is less than or equal to',
        'lt': 'is less than',
        'gte': 'is greater than or equal to',
        'gt': 'is greater than',
    }

    def generate_lookup(self, field, lookup):
        if not lookup:
            return field
        return '{}__{}'.format(field, lookup)

    def generate_lookups(self):
        lookups = []
        for field, querytypes in self.filter_fields.items():
            for lookup in querytypes:
                lookups.append(self.generate_lookup(field, lookup))
        return lookups

    def generate_labels(self):
        labels = {}
        for field, querytypes in self.filter_fields.items():
            for lookup in querytypes:
                key = self.generate_lookup(field, lookup)
                label = '{} {}'.format(field, self.query_label.get(lookup, '')).strip()
                labels[key] = label
        return labels

    def get_queries(self, request):
        lookups = self.generate_lookups()
        terms = []
        for key, value in request.query_params.items():
            if key in lookups and value:
                terms.append((key, value))
        return terms

    def filter_queryset(self, request, queryset, view):
        fields = self.filter_fields
        terms = self.get_queries(request)

        if not (fields and terms):
            return queryset

        orm_lookups = {k: v for k, v in terms}
        try:
            queryset = queryset.filter(**orm_lookups)
        except DjangoValidationError as errors:
            errormsg = '\n'.join(errors)
            raise DRFValidationError(errormsg)
        return queryset

    def to_html(self, request, queryset, view):
        if not self.filter_fields:
            return ''

        lookups = sorted(self.generate_lookups())
        terms = dict(self.get_queries(request))
        fields = OrderedDict()
        labels = self.generate_labels()
        for key in lookups:
            fields[key] = {'label': labels[key], 'value': terms.get(key, '')}

        context = {'fields': fields}
        template = loader.get_template(self.template)
        return template.render(context)


class FilterMixin:
    filter_backends = (SearchFilter, OrderingFilter, EEStoreRepoFilterBackend)
    search_fields = ('pid', 'remote_pid', 'name', 'uri')
    ordering_fields = ('name', 'source', 'last_fetched')
    ordering = ('name',)
