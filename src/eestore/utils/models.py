from django.db import models


class AbstractRepo(models.Model):
    pid = models.CharField(
        help_text="EEStore persistent id", max_length=255)
    remote_id = models.CharField(
        help_text="Remote id", max_length=255, blank=True)
    remote_pid = models.CharField(
        help_text="Remote persistent id, if any", max_length=255, blank=True)
    name = models.CharField(max_length=255)
    uri = models.URLField(blank=True)

    source = models.CharField(max_length=64)
    first_fetched = models.DateTimeField(auto_now_add=True)
    last_fetched = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        super(AbstractRepo, self).save(*args, **kwargs)
        if not self.last_fetched:
            self.last_fetched = self.first_fetched
            self.save()

    def __str__(self):
        return self.pid
