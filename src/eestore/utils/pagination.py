from rest_framework_json_api.pagination import PageNumberPagination


__all__ = [
    'StandardPagination',
]

class StandardPagination(PageNumberPagination):
    page_size_query_param = 'max'
    max_page_size = 1000
