from datetime import datetime


__all__ = [
    'generate_pid',
    'from_compact_iso8601',
    'to_compact_iso8601',
]


UTC_ISODATE_FORMAT = '%Y%m%dT%H%M%SZ'


def generate_pid(repotype, source, remote_id):
    return '{}:{}/{}'.format(repotype, source, remote_id)


def to_compact_iso8601(timestamp):
    return datetime.strftime(timestamp, UTC_ISODATE_FORMAT)


def from_compact_iso8601(string):
    string = string[:-1]+'+0000'
    format_string = UTC_ISODATE_FORMAT[:-1]+'%z'
    return datetime.strptime(string, format_string)
