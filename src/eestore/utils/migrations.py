from eestore.utils import generate_pid


def rename_local_source_to_eestore(model):
    for obj in model.objects.filter(source='local'):
        obj.source = 'eestore'
        obj.save()


def rename_eestore_source_to_local(model):
    for obj in model.objects.filter(source='eestore'):
        obj.source = 'local'
        obj.save()


def set_remote_id_from_pid(model):
    repotype = model.__name__.rsplit('.', 1)[-1].lower()
    for obj in model.objects.all():
        obj.remote_id = obj.pid
        obj.pid = generate_pid(repotype, obj.source, obj.remote_id)
        obj.save()


def unset_remote_id(model):
    for obj in model.objects.all():
        obj.pid = obj.remote_id
        obj.remote_id = ''
        obj.save()
