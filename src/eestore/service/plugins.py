import requests

from eestore.plugins import PluginMount, BaseAPIPlugin

from .models import Service

__all__ = [
    'BaseServicePlugin',
    'LocalServicePlugin',
]


class BaseServicePlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all Service plugins

    Attributes:

    plugintype          'service'
    model               Service
    """
    plugintype = 'service'
    model = Service
