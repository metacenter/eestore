from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import Service


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = STANDARD_FIELDS + ['description']


class ServiceViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    search_fields = FilterMixin.search_fields + ('description',)
