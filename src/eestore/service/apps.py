from django.apps import AppConfig


class EEStoreServiceConfig(AppConfig):
    name = 'eestore.service'
    label = 'eestore_service'
    verbose_name = 'Service'
