from django.db import models

from ..utils.models import AbstractRepo


class Service(AbstractRepo):
    description = models.TextField(blank=True)
