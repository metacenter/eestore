from django.contrib import admin

from .models import DatasetMdRepo


class DatasetMdRepoAdmin(admin.ModelAdmin):
    date_hierarchy = 'last_fetched'
    list_display = ('name', 'source', 'pid', 'last_fetched')
    list_filter = ('source',)
    readonly_fields = ('first_fetched',)


admin.site.register(DatasetMdRepo, DatasetMdRepoAdmin)
