from pathlib import Path

import yaml

from eestore.plugins import PluginMount
from eestore.plugins import BaseAPIPlugin

from .models import DatasetMdRepo

__all__ = [
    'BaseDatasetMdRepoPlugin',
    'LocalDatasetMdRepoPlugin',
]


class BaseDatasetMdRepoPlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all DatasetMdRepo plugins

    Attributes:

    plugintype          'datasetmdrepo'
    model               DatasetMdRepo
    """
    plugintype = 'datasetmdrepo'
    model = DatasetMdRepo
