from django.db import models

from ..utils.models import AbstractRepo


class DatasetMdRepo(AbstractRepo):
    pass

    class Meta:
        verbose_name = 'Dataset Metadata Repo'
