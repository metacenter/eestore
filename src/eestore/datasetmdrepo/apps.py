from django.apps import AppConfig


class EEStoreDatasetMdRepoConfig(AppConfig):
    name = 'eestore.datasetmdrepo'
    label = 'eestore_datasetmdrepo'
    verbose_name = 'Dataset Metadata Repo'
