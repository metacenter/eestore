from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import DatasetMdRepo


class DatasetMdRepoSerializer(serializers.ModelSerializer):
    class Meta:
        model = DatasetMdRepo
        fields = STANDARD_FIELDS


class DatasetMdRepoViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = DatasetMdRepo.objects.all()
    serializer_class = DatasetMdRepoSerializer
