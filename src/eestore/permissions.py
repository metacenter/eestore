from rest_framework.permissions import IsAuthenticated, IsAdminUser


__all__ = [
    'AuthenticatedSuperuserPermission',
]


class AuthenticatedSuperuserPermission(IsAuthenticated and IsAdminUser):

    def has_permission(self, request, view):
        parents = super().has_permission(request, view)
        return parents and request.user.is_superuser
