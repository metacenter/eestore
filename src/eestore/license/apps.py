from django.apps import AppConfig


class EEStoreLicenseConfig(AppConfig):
    name = 'eestore.license'
    label = 'eestore_license'
    verbose_name = 'License'
