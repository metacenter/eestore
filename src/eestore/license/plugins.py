from io import StringIO
import csv
from uuid import uuid4 as uuid

from django.utils.timezone import now as utcnow

import requests

from eestore.plugins import PluginMount, BaseAPIPlugin
from eestore.utils import generate_pid

from .models import License

__all__ = [
    'BaseLicensePlugin',
    'LocalLicensePlugin',
    'CreativeCommonsLicensePlugin',
]


class BaseLicensePlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all License plugins

    Attributes:

    plugintype          'license'
    model               License
    """
    plugintype = 'license'
    model = License


class CreativeCommonsLicensePlugin(BaseLicensePlugin):
    pluginname = 'creativecommons'
    browseable = False
    source_endpoint = None
    url_format = None
    readonly = True
    auth_needed = False
    queryable_by_date = False
