from django.db import models

from ..utils.models import AbstractRepo


class License(AbstractRepo):
    abbreviation = models.CharField(max_length=16, blank=True)
    version = models.CharField(max_length=16, blank=True)

    class Meta:
        db_table = 'eestore_license'
