from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import License


class LicenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = License
        fields = STANDARD_FIELDS + ['version', 'abbreviation']


class LicenseViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = License.objects.all()
    serializer_class = LicenseSerializer
    search_fields = FilterMixin.search_fields + ('version', 'abbreviation',)
