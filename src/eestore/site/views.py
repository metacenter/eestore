from django.views.generic import TemplateView

from eestore import __version__


class HomePageView(TemplateView):
    template_name = 'theme/homepage.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['version'] = __version__
        context['hostname'] = self.request.build_absolute_uri('')
        # TODO: link to public source code
        return context
