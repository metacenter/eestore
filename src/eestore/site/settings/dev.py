# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

import os

import dj_database_url

from .base import *


# SECURITY WARNING: keep the secret key used in production secret!
FALLBACK_SECRET_KEY = '#(-%2kkvfx4a9x%mu3^bbhmzmly9ugfsf^b52_fd#f)to0i%4h'
SECRET_KEY = os.environ.get('SECRET_KEY', FALLBACK_SECRET_KEY)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    '0.0.0.0',
    '127.0.0.1',
    'localhost',
]

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASE_URL = os.environ.get('DATABASE_URL', None)

if DATABASE_URL:
    DEFAULT = dj_database_url.parse(DATABASE_URL)
else:
    DEFAULT = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }

DATABASES = {
    'default': DEFAULT,
}


# Templates. Themes-directory can be added to 'DIRS', first found wins

# TEMPLATES = [
#     {
#         'BACKEND': 'django.template.backends.django.DjangoTemplates',
#         'DIRS': [
#             pathjoin(SITE_DIR, 'templates'),
#         ],
#         'APP_DIRS': True,
#         'OPTIONS': {
#             'context_processors': [
#                 'django.template.context_processors.debug',
#                 'django.template.context_processors.request',
#                 'django.contrib.auth.context_processors.auth',
#                 'django.contrib.messages.context_processors.messages',
#             ],
#         },
#     },
# ]
#
# WSGI_APPLICATION = 'eestore.site.wsgi.application'


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

# STATIC_URL = '/static/'

# Themes-directory can be added here, first found wins
STATICFILES_DIRS = [
    pathjoin(SITE_DIR, 'static'),
]

STATIC_ROOT = pathjoin(BASE_DIR, 'collected_static_files')

INSTALLED_APPS += ['rest_framework.authtoken']
REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] += [
    'rest_framework.authentication.SessionAuthentication',
    'eestore.auth.authentication.BearerTokenAuthentication',
]
