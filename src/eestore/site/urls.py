"""eestore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

SHORTNAME_RE = r'(?P<pk>\w+:\w+)'
TIMESTAMP_RE = r'(?P<timestamp>\d{8}T\d{6}Z)'

# Non-API
from eestore.site.views import HomePageView

# sources
from eestore.datarepo.views import DataRepoViewSet
from eestore.projectrepo.views import ProjectRepoViewSet
from eestore.organizationrepo.views import OrganizationRepoViewSet
from eestore.metadataschema.views import MetadataSchemaViewSet
from eestore.servicerepo.views import ServiceRepoViewSet
from eestore.service.views import ServiceViewSet
from eestore.personrepo.views import PersonRepoViewSet
from eestore.fileformat.views import FileFormatViewSet
from eestore.license.views import LicenseViewSet
from eestore.datasetmdrepo.views import DatasetMdRepoViewSet
from eestore.taxonomy.views import TaxonomyViewSet

# other
from eestore.plugins.views import PluginViewSet, PluginRefetchProgressView

router = routers.DefaultRouter()
router.register('datarepo', DataRepoViewSet)
router.register('projectrepo', ProjectRepoViewSet)
router.register('organizationrepo', OrganizationRepoViewSet)
router.register('metadataschema', MetadataSchemaViewSet)
router.register('servicerepo', ServiceRepoViewSet)
router.register('service', ServiceViewSet)
router.register('personrepo', PersonRepoViewSet)
router.register('fileformat', FileFormatViewSet)
router.register('license', LicenseViewSet)
router.register('datasetmdrepo', DatasetMdRepoViewSet)
router.register('taxonomy', TaxonomyViewSet)
router.register('plugin', PluginViewSet, basename='plugin')

urlpatterns = [
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^admin/', admin.site.urls),
    url(r'^api/plugin/' + SHORTNAME_RE + '/refetch/' + TIMESTAMP_RE + '/$', PluginRefetchProgressView.as_view(), name='plugin-refetch-progress'),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/token/', obtain_auth_token),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
