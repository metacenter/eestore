from pathlib import Path

import yaml

from eestore.plugins import PluginMount
from eestore.plugins import BaseAPIPlugin
from eestore.plugins import git
from eestore.utils import generate_pid

from .models import MetadataSchema

__all__ = [
    'BaseMetadataSchemaPlugin',
    'LocalMetadataSchemaPlugin',
]


class BaseMetadataSchemaPlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all MetadataSchema plugins

    Attributes:

    plugintype          'metadataschema'
    model               MetadataSchema
    """
    plugintype = 'metadataschema'
    model = MetadataSchema


class RDAllianceMetadataSchemaPlugin(BaseMetadataSchemaPlugin):
    pluginname = 'rd-alliance'
    readonly = True
    browseable = True
    queryable_by_date = False
    source_endpoint = 'https://github.com/rd-alliance/metadata-directory.git'
    path = '/tmp/rd-alliance'
    ignore_files = {'index.md', 'add.md'}

    def get_all_data(self, after=None):
        ok_clone = git.clone(self.source_endpoint, self.path)
        ok_pull = git.pull(self.path)
        data = []
        if ok_clone and ok_pull:
            path = Path(self.path) / 'standards'
            for standard_path in path.iterdir():
                standard_filename = str(standard_path.name)
                if standard_filename in self.ignore_files:
                    continue
                with standard_path.open('r') as F:
                    rawyaml = F.read()
                    obj = next(yaml.safe_load_all(rawyaml))
                    data.append(self.get_data_for_single_repo(obj))
        return data

    def get_data_for_single_repo(self, obj):
        data = {}
        data['source'] = self.pluginname
        pid = obj.get('slug', '')
        if not pid:
            assert False, obj
        data['remote_id'] = pid
        data['remote_pid'] = pid
        data['pid'] = generate_pid(self.plugintype, self.pluginname, data['remote_id'])
        name = obj['title']
        data['name'] = name
        url = obj.get('website', '')
        data['uri'] = url
        return data
