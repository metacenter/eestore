from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import MetadataSchema


class MetadataSchemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetadataSchema
        fields = STANDARD_FIELDS


class MetadataSchemaViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = MetadataSchema.objects.all()
    serializer_class = MetadataSchemaSerializer
