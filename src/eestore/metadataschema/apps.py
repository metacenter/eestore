from django.apps import AppConfig


class EEStoreMetadataSchemaConfig(AppConfig):
    name = 'eestore.metadataschema'
    label = 'eestore_metadataschema'
    verbose_name = 'Metadata Schema'
