from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import ServiceRepo


class ServiceRepoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceRepo
        fields = STANDARD_FIELDS


class ServiceRepoViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = ServiceRepo.objects.all()
    serializer_class = ServiceRepoSerializer
