from django.apps import AppConfig


class EEStoreServicerepoConfig(AppConfig):
    name = 'eestore.servicerepo'
    label = 'eestore_servicerepo'
    verbose_name = 'Service Repository'
