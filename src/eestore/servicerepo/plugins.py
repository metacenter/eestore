from eestore.plugins import PluginMount, BaseAPIPlugin

from .models import ServiceRepo

__all__ = [
    'BaseServiceRepoPlugin',
    'LocalServiceRepoPlugin',
]


class BaseServiceRepoPlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all ServiceRepo plugins

    Attributes:

    plugintype          'servicerepo'
    model               ServiceRepo
    """
    plugintype = 'servicerepo'
    model = ServiceRepo
