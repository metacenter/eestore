from django.apps import AppConfig


class EEStorePersonrepoConfig(AppConfig):
    name = 'eestore.personrepo'
    label = 'eestore_personrepo'
    verbose_name = 'Person Repository'
