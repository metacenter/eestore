from rest_framework import serializers
from rest_framework import viewsets

from ..utils.views import FilterMixin, STANDARD_FIELDS
from .models import PersonRepo


class PersonRepoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonRepo
        fields = STANDARD_FIELDS + ['email']


class PersonRepoViewSet(FilterMixin, viewsets.ReadOnlyModelViewSet):
    queryset = PersonRepo.objects.all()
    serializer_class = PersonRepoSerializer
    search_fields = FilterMixin.search_fields + ('email',)
