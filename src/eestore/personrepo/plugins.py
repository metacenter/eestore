from eestore.plugins import PluginMount, BaseAPIPlugin
from eestore.plugins.utils import trunc_name
from eestore.eapis.cristin import CRISTINV2Mixin
from eestore.utils import generate_pid


from .models import PersonRepo

__all__ = [
    'BasePersonRepoPlugin',
    'LocalPersonRepoPlugin',
    'CRISTINPersonRepoPlugin',
]


class BasePersonRepoPlugin(BaseAPIPlugin, metaclass=PluginMount):
    """
    Abstract base class for all PersonRepo plugins

    Attributes:

    plugintype          'personrepo'
    model               PersonRepo
    """
    plugintype = 'personrepo'
    model = PersonRepo


class CRISTINPersonRepoPlugin(CRISTINV2Mixin, BasePersonRepoPlugin):
    source_endpoint = 'http://api.cristin.no/v2/persons'
    url_format = 'https://wo.cristin.no/as/WebObjects/cristin.woa/wa/personVis?type=PERSON&pnr={}'

    def get_data_for_single_repo(self, jsonblob):
        assert type(jsonblob) in (list, dict), 'Wrong format for input'
        data = {}
        data['source'] = self.pluginname
        data['remote_id'] = jsonblob['cristin_person_id']
        data['pid'] = generate_pid(self.plugintype, data['source'], data['remote_id'])
        firstname = jsonblob.get('firstname', '')
        lastname = jsonblob.get('surname', '')
        name = ' '.join((firstname, lastname))
        data['name'] = trunc_name(name)
        url = self.get_url(pid)
        if url:
            data['uri'] = url
        return data
