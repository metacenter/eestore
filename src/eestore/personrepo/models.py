from django.db import models

from ..utils.models import AbstractRepo


class PersonRepo(AbstractRepo):
    email = models.EmailField(blank=True)
