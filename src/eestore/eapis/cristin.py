"""CRIStin specific helpers"""

import requests

from django.utils.timezone import now as utcnow

from eestore.plugins.utils import dumpjson, dedup_url


__all__ = [
    'CRISTINV2Mixin',
]


class CRISTINV2Mixin:
    pluginname = 'cristin'
    readonly = True
    auth_needed = False
    browseable = True
    language_priority = ('en', 'nb', 'nn')
    queryable_by_date = False

    def get_single_page(self, link, params=None):
        params = params if params else {}
        link = dedup_url(link, singles=('lang',))
        r = requests.get(link, params=params)
        try:
            next_page = r.links['next']['url']
        except KeyError:
            next_page = None
        jsonblob = r.json()
        if 'errors' in jsonblob:
            raise ValueError(jsonblob)
        return jsonblob, next_page

    def get_all_data(self, after=None):
        # CRISTIN paginates
        params = {
            'per_page': 1000
        }
        data = []
        try:
            jsonblob, next_page = self.get_single_page(
                self.source_endpoint,
                params=params,
            )
        except ValueError as e:
            assert False, e
        for repo in jsonblob:
            data.append(self.get_data_for_single_repo(repo))
        while next_page:
            try_page = next_page
            try:
                jsonblob, next_page = self.get_single_page(
                    try_page,
                )
            except ValueError as e:
                assert False, e
            for repo in jsonblob:
                data.append(self.get_data_for_single_repo(repo))
        return data

    def lookup(self, pid):
        r = requests.get(pathjoin(self.source_endpoint, pid))
        jsonblob = r.json
        return jsonblob

    def _select_language_object(self, jsonblob, fieldname):
        # Cristin has multilanguage fields
        language_object = jsonblob[fieldname]

        # If there's only one, we're done
        if len(language_object) == 1:
            return tuple(language_object.values())[0]

        # First try the preferred language, if any
        main_language = jsonblob.get('main_language', None)
        if main_language:
            return language_object.get(main_language, None)

        # Else pick the first available language
        for langcode in self.language_priority:
            obj = language_object.get(langcode, None)
            if obj:
                return obj
        return None
