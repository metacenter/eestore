from django.conf import settings


__all__ = [
    'get_enabled_plugins',
]


DEFAULT_PLUGINS = [
    'eestore.datarepo.plugins.LocalDataRepoPlugin',
    'eestore.datarepo.plugins.Re3DataRepoPlugin',
]


def get_enabled_plugins():
    if not hasattr(settings, 'EESTORE_PLUGINS'):
        return DEFAULT_PLUGINS[:]
    return settings.EESTORE_PLUGINS[:]
