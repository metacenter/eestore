import importlib
from collections import OrderedDict
from multiprocessing import Process
from datetime import timedelta

import django
from django.conf import settings
from django.db import transaction, connections
from django.utils.module_loading import import_string
from django.utils.timezone import now as utcnow

from .utils import dumpjson

default_app_config = 'eestore.plugins.apps.EEStorePluginConfig'


__all__ = [
    'PluginMount',
    'BaseAPIPlugin',
    'load_plugins',
    'unload_plugins',
    'get_enabled_plugins',
    'PluginError',
    'TYPED_LOADED_PLUGINS',
]

DEFAULT_PLUGINS = [
    'eestore.datarepo.plugins.Re3DataRepoPlugin',
    'eestore.projectrepo.plugins.CRISTINProjectRepoPlugin',
]

LOADED_PLUGINS = {}
TYPED_LOADED_PLUGINS = {}
PLUGIN_SHORTNAMES = {}


class PluginError(Exception):
    pass


class PluginMount(type):
    """
    A type for plugins

    Inherit from this to make a new plugin type, then inherit from
    that to make a plugin. The plugin needs to register itself,
    this to avoid auto-registration on import. Explicit is better
    than implicit.


    Inspired by Marty Alchin's simple plugin framework:

    http://martyalchin.com/2008/jan/10/simple-plugin-framework/

    (Url valid as of 2017-07-03)
    """

    def __init__(cls, name, bases, attrs):
        if not hasattr(cls, 'plugins'):
            cls.plugins = OrderedDict()

    def register(cls):
        cls.plugins[cls.pluginname] = cls

    def unregister(cls):
        try:
            del cls.plugins[cls.pluginname]
        except KeyError:
            # not registered so alles ok
            pass


class BaseAPIPlugin:
    """
    Abstract base class for all API plugins

    Attributes:

    plugintype          Type of plugin. String, variable name safe.
    pluginname          Name of plugin, unique for type. String, variable name safe.
    source_endpoint     Url to endpoint of API or empty string
    url_format          f-string that build an url to an html-representation
    readonly            Whether the API is readonly or not. Boolean.
    auth_needed         Whether it is necessary to log in to use the API. Booelan.
    model               Django model class that caches the fetched data
    browseable          Whether it is possible to fetch all in one go. Boolean.
    queryable_by_date   Whether one can fetch a subset newer than a specific date. Boolean.
    """

    plugintype = None
    pluginname = None
    source_endpoint = None
    url_format = None
    readonly = None
    auth_needed = None
    browseable = None
    queryable_by_date = None

    @property
    def shortname(self):
        return make_shortname(self)

    @property
    def pluginrow(self):
        Plugin = django.apps.apps.get_model(
            app_label='eestore_plugins', model_name='Plugin'
        )
        return Plugin.objects.get(shortname=self.shortname)

    def _ensure_not_None(self, *args):
        if not args:
            args = ('plugintype', 'pluginname', 'source_endpoint', 'readonly',
                    'auth_needed', 'browseable')
        nones = []
        for arg in args:
            if getattr(self, arg, None) is None:
                nones.append(arg)
        if nones:
            raise NotImplementedError(
                'The following arguments have not been set: {}',
                ', '.join(nones)
            )

    def get_url(self, pid):
        if not self.url_format:
            return ''
        return self.url_format.format(str(pid))

    def update_urls(self):
        "Update stored urls to current format"
        for item in self.get_queryset():
            url = self.get_url(item.pid)
            item.uri = url
            item.save()

    def get_all(self):
        if self.browseable:
            self.fill()
            objs = self.get_queryset()
            return dumpjson(objs.values())
        raise TypeError('Not possible/allowed to get all')

    def query(self, query):
        raise NotImplementedError

    def lookup(self, pid):
        raise NotImplementedError

    def get_queryset(self, lock=False):
        qs = self.model.objects
        if lock:
            qs = self.model.objects.select_for_update(skip_locked=True)
        return qs.filter(source=self.pluginname)
    _get_qs = get_queryset

    def to_json(self, obj):
        data = vars(obj)
        data.pop('_state', None)
        data['type'] = self.plugintype
        return dumpjson(data)

    def set_type(self, values):
        for repo in values:
            repo['type'] = self.plugintype
        return values

    def refetch(self, data=None, timestamp=None):
        if not data:
            data = self.get_all_data()
        now = utcnow() if not timestamp else timestamp
        pluginrow = self.pluginrow
        previous_update = pluginrow.last_fetched
        # 1 day is absolute minimum
        # TODO: Have this configurable
        if previous_update and previous_update - now < timedelta(days=1):
            msg = "Will not refetch, too early since last time: {}".format(
                previous_update
            )
            raise PluginError(msg)
        qs = self.get_queryset(lock=True)
        new = 0
        updated = 0
        with transaction.atomic():
            for repo in data:
                pid = repo['pid']
                repo['last_fetched'] = now
                _, created = qs.update_or_create(pid=pid, defaults=repo)
                if created:
                    new += 1
                else:
                    updated += 1
        pluginrow.last_fetched = now
        pluginrow.save()
        return {'new': new, 'updated': updated}
    fill = refetch
    refetch_all = refetch

    def get_all(self):
        self.fill()
        objs = self.get_queryset()
        return dumpjson(objs.values())


def make_shortname(pluginclass):
    return '{}:{}'.format(pluginclass.plugintype, pluginclass.pluginname)


def load_plugins(plugin_dotted_paths):
    "Load and enable all plugins found in an iterable of dotted paths"

    for plugin in plugin_dotted_paths:
        pluginclass = import_string(plugin)
        pluginclass.register()
        LOADED_PLUGINS[plugin] = pluginclass
        shortname = make_shortname(pluginclass)
        PLUGIN_SHORTNAMES[shortname] = pluginclass

    for pluginclass in LOADED_PLUGINS.values():
        plugintype = pluginclass.plugintype
        plugins = pluginclass.plugins
        TYPED_LOADED_PLUGINS.setdefault(plugintype, set()).update(plugins)


def unload_plugins(plugin_dotted_paths):
    "Unload and disable all plugins found in an iterable of dotted paths"

    for plugin in plugin_dotted_paths:
        pluginclass = LOADED_PLUGINS[plugin]
        pluginclass.unregister()
        del LOADED_PLUGINS[plugin]
        plugintype = pluginclass.plugintype
        TYPED_LOADED_PLUGINS[k].discard(pluginclass)
        shortname = make_shortname(pluginclass)
        del PLUGIN_SHORTNAMES[shortname]

    for k in tuple(TYPED_LOADED_PLUGINS):
        if not TYPED_LOADED_PLUGINS[k]:
            del TYPED_LOADED_PLUGINS[k]


def get_enabled_plugins():
    "Get enabled plugins (dotted paths) from settings"

    return getattr(settings, 'EESTORE_PLUGINS', DEFAULT_PLUGINS)


def get_pluginclass_from_dotted_path(plugin_dotted_path):
    try:
        pluginclass = LOADED_PLUGINS[plugin_dotted_path]
    except KeyError:
        raise PluginError('Plugin "{}" not loaded.'.format(plugin_dotted_path))
    return pluginclass


def get_pluginclass_from_shortname(shortname):
    try:
        pluginclass = PLUGIN_SHORTNAMES[shortname]
    except KeyError:
        raise PluginError('Plugin with shortname "{}" not loaded.'.format(shortname))
    return pluginclass


def get_pluginclass(dotted_or_shortname):
    try:
        if '.' in dotted_or_shortname:
            return get_pluginclass_from_dotted_path(dotted_or_shortname)
        if ':' in dotted_or_shortname:
            return get_pluginclass_from_shortname(dotted_or_shortname)
    except PluginError:
        raise
    raise PluginError('Input {} is neither a dotted path nor a shortname'.format(
        dotted_or_shortname
    ))


def refetch(pluginclass, noop=None, timestamp=None):
    if pluginclass.pluginname in ('eestore', 'local'):
        raise PluginError('Plugin {} is for local data and cannot be refetched'.format(dotted_or_shortname))
    if not noop:
        plugin = pluginclass()
        results = plugin.refetch(timestamp=timestamp)
        return results


def background_refetch(pluginclass, noop=None, timestamp=None):
    connections.close_all()
    p = Process(target=refetch, args=(pluginclass,),
                kwargs={'noop': noop, 'timestamp': timestamp})
    p.start()
    return p


def refetch_data_for_plugin(dotted_or_shortname, noop=False, timestamp=None):
    pluginclass = get_pluginclass(dotted_or_shortname)
    return refetch(pluginclass, noop, timestamp)
