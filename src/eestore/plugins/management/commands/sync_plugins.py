from django.core.management.base import CommandError

from eestore.plugins.models import sync_plugins_with_feedback
from eestore.utils.commands import EEStoreBaseCommand


class Command(EEStoreBaseCommand):
    help = "Syncs the Plugin table with loaded plugins"

    def handle(self, *args, **options):
        results = sync_plugins_with_feedback()

        if options['verbosity']:
            self.stdout.write("Syncing plugins from source to database..")
            self.stdout.write("")
        if options['verbosity'] > 1:
            if results['unloaded'] or results['new']:
                self.stdout.write("Unloaded:")
                for p in results['unloaded']:
                    self.stdout.write("\t{}".format(p))
                self.stdout.write("New:")
                for p in results['new']:
                    self.stdout.write("\t{}".format(p))
            else:
                self.stdout.write("No changes.")
            self.stdout.write("")
        if options['verbosity']:
            self.stdout.write("Plugins active after sync:")
            for p in results['after']:
                self.stdout.write("\t{}".format(p))
