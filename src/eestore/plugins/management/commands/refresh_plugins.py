from django.core.management.base import CommandError

from eestore.plugins import get_enabled_plugins
from eestore.plugins import refetch_data_for_plugin
from eestore.plugins import PluginError
from eestore.utils.commands import EEStoreBaseCommand


class Command(EEStoreBaseCommand):
    help = "Refreshes the data of the given (or all) plugins, if possible"

    def add_arguments(self, parser):
        parser.add_argument('-n', '--noop', action='store_true',
                            help="Don't actually refresh")

        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument('plugins', nargs='*', type=str, default=[],
                           help="One or more shortnames of plugins"
                           )
        group.add_argument('-a', '--all', action='store_true',
                           help="Refresh all loaded plugins"
                           )

    def handle(self, *args, **options):
        ok = 0
        noop = options['noop']
        plugins = options.get('plugins', None)
        if options['all']:
            plugins = get_enabled_plugins()

        for plugin in plugins:
            try:
                results = refetch_data_for_plugin(plugin, noop=noop)
                if options['verbosity'] > 1:
                    self.stdout.write('Refreshed {}'.format(plugin))
                    if results:
                        self.stdout.write('New: {}'.format(results['new']))
                        self.stdout.write('Updated: {}'.format(results['updated']))
                        self.stdout.write()
                ok += 1
            except PluginError as e:
                self.stderr.write('NB! {}'.format(e))
        if options['verbosity'] > 0:
            self.stdout.write('Successfully refreshed {} of {} plugins'.format(ok, len(plugins)))
