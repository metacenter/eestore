from django.core.management.base import CommandError

from eestore.plugins import get_enabled_plugins
from eestore.plugins import make_shortname
from eestore.plugins import LOADED_PLUGINS
from eestore.utils.commands import EEStoreBaseCommand


class Command(EEStoreBaseCommand):
    help = "List all loaded plugins"

    def handle(self, *args, **options):
        plugins_in_settings = get_enabled_plugins()

        for plugin_dotted_path, pluginclass in sorted(LOADED_PLUGINS.items()):
            shortname = make_shortname(pluginclass)
            format = '{} ({}) *' if plugin_dotted_path in plugins_in_settings else '{} ({})'
            self.stdout.write(format.format(plugin_dotted_path, shortname))
