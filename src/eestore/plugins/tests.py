from unittest import TestCase

from . import BaseAPIPlugin


class BaseAPIPluginTest(TestCase):

    def test__ensure_not_None(self):
        with self.assertRaises(NotImplementedError):
            BaseAPIPlugin()._ensure_not_None()
