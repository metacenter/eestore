from django.shortcuts import get_object_or_404
from django.utils.timezone import now as utcnow

from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import views
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.reverse import reverse

from eestore.permissions import AuthenticatedSuperuserPermission
from eestore.utils import from_compact_iso8601, to_compact_iso8601
from eestore.utils.responses import AcceptedResponse
from eestore.utils.responses import Http404Response
from eestore.utils.responses import SeeOtherResponse

from . import background_refetch
from .models import Plugin, sync_plugins_with_feedback


class PluginSerializer(serializers.Serializer):
    shortname = serializers.CharField(max_length=255, allow_blank=False,
                                      read_only=True)
    plugin_type = serializers.SlugField(max_length=32, allow_blank=False,
                                        read_only=True)
    plugin_name = serializers.CharField(max_length=255, allow_blank=False,
                                        read_only=True)
    last_fetched = serializers.DateTimeField(read_only=True, allow_null=True)

    source_endpoint = serializers.URLField(max_length=255, read_only=True,
                                           allow_null=True)
    url_format = serializers.CharField(max_length=255, read_only=True,
                                       allow_null=True)
    readonly = serializers.BooleanField(read_only=True, allow_null=True)
    auth_needed = serializers.BooleanField(read_only=True, allow_null=True)
    browseable = serializers.BooleanField(read_only=True, allow_null=True)
    queryable_by_date = serializers.BooleanField(read_only=True,
                                                 allow_null=True)


class PluginViewSet(viewsets.GenericViewSet):
    serializer_class = PluginSerializer

    def get_queryset(self):
        return Plugin.objects.only_loaded()

    @action(detail=False, methods=['post'],
            permission_classes=[AuthenticatedSuperuserPermission])
    def sync(self, request):
        result = sync_plugins_with_feedback()
        see_other = reverse('plugin-list')
        return SeeOtherResponse(see_other, data=result)

    @action(detail=True, methods=['post'],
            permission_classes=[AuthenticatedSuperuserPermission])
    def refetch(self, request, pk=None):
        queryset = self.get_queryset()
        item = get_object_or_404(queryset, pk=pk)
        item.set_properties()
        if item.is_loaded:
            now = utcnow()
            pluginclass = item.plugin
            background_refetch(pluginclass, timestamp=now)
            timestamp = to_compact_iso8601(now)
            location = reverse(
                'plugin-refetch-progress',
                kwargs={'pk': pk, 'timestamp': timestamp}
            )
            return AcceptedResponse(location)
        return Http404Response()
    fill = refetch

    def list(self, request):
        queryset = self.get_queryset().add_properties()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = self.get_queryset()
        item = get_object_or_404(queryset, pk=pk)
        item.set_properties()
        serializer = self.serializer_class(item)
        return Response(serializer.data)


class PluginRefetchProgressView(views.APIView):

    def get(self, request, format=None, **kwargs):
        shortname = kwargs['pk']
        timestamp_string = kwargs['timestamp']
        timestamp = from_compact_iso8601(timestamp_string)
        try:
            plugin = Plugin.objects.get(shortname=shortname)
        except Plugin.DoesNotExist:
            return Http404Response()
        if plugin.last_fetched >= timestamp:
            # Go to correct endpoint with SeeOtherResponse when sources are in
            return Response()
        location = reverse(
                'plugin-refetch-progress',
                kwargs={'pk': shortname, 'timestamp': timestamp_string}
        )
        return AcceptedResponse(location)
    post = get  # Simplify manual debugging. The refetch will not be restarted!
