import requests


class HTTPBasicAuthenticationPluginMixin:
    """Log in to a source with HTTP Basic Authentication

    Needs a username and a password
    """

    def set_credentials(self, username, password):
        self.username = username
        self.password = password

    def setup_request(self, url):
        assert self.username and self.password, 'Attributes "username" and "password" must be set'
        r = requests.get(url, auth=(self.username, self.password))
        r.raise_for_status() # Exception if status_code not 2xx
        return r
