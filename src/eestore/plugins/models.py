from django.db import models
from django.apps import apps

from eestore.plugins import PluginError
from eestore.plugins import get_pluginclass_from_shortname
from eestore.plugins import PLUGIN_SHORTNAMES


def sync_plugins_with_feedback():
    "Sync plugins and report any changes"
    before = set(Plugin.objects.values_list('shortname', flat=True))
    Plugin.objects.sync()
    after = set(Plugin.objects.values_list('shortname', flat=True))

    unloaded = before - after
    new = after - before
    return {
        'before': sorted(before),
        'after': sorted(after),
        'unloaded': sorted(unloaded),
        'new': sorted(new),
    }


class PluginQuerySet(models.QuerySet):

    def add_properties(self):
        qs = self.all()
        for plugin in qs:
            plugin.set_properties()
        return qs

    def only_loaded(self):
        return self.filter(shortname__in=PLUGIN_SHORTNAMES.keys())

    def only_unloaded(self):
        return self.filter(shortname__not_in=PLUGIN_SHORTNAMES.keys())


class PluginManager(models.Manager):

    def sync(self):
        for shortname in PLUGIN_SHORTNAMES.keys():
            type_, name = shortname.split(':')
            self.get_or_create(
                shortname=shortname,
                plugin_name=name,
                plugin_type=type_,
            )


class Plugin(models.Model):
    PLUGIN_PROPERTIES = (
        'source_endpoint',
        'url_format',
        'readonly',
        'auth_needed',
        'browseable',
        'queryable_by_date',
    )
    shortname = models.SlugField(max_length=255, primary_key=True)
    # Denormalize for speedy filtering
    plugin_type = models.SlugField(max_length=32)
    plugin_name = models.SlugField(max_length=255)
    last_fetched = models.DateTimeField(blank=True, null=True)

    objects = PluginManager.from_queryset(PluginQuerySet)()

    def __str__(self):
        return self.shortname

    @property
    def model(self):
        return self.plugin.model

    @property
    def plugin(self):
        return get_pluginclass_from_shortname(self.shortname)

    @property
    def is_loaded(self):
        try:
            self.plugin
        except PluginError:
            return False
        else:
            return True
        # uncaught exception
        # ought to log if we ever end up here..

    def set_properties(self):
        for p in self.PLUGIN_PROPERTIES:
            if self.is_loaded:
                setattr(self, p, getattr(self.plugin, p, None))
            else:
                setattr(self, p, None)
