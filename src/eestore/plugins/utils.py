import json
from urllib.parse import urlparse, parse_qs, urlencode, urlunparse

from django.core.serializers.json import DjangoJSONEncoder


def dumpjson(data, **kwargs):
    kwargs.pop('cls', None)
    return json.dumps(data, cls=DjangoJSONEncoder, **kwargs)


def trunc_name(name):
    return name[:255]


def dedup_query(query, singles=None, ignore=None, remove=None):
    "Remove duplicates in querystring"
    query = parse_qs(query)
    keep = {}
    query_list = []
    for kv in tuple(query.keys()):
        if kv in remove:
            query.pop(kv, None)
            continue
        if kv in ignore:
            to_keep = query.pop(kv, None)
            if to_keep:
                keep[kv] = to_keep
        elif kv in singles:
            wrong = query.pop(kv, None)
            if wrong:
                keep[kv] = tuple(set(wrong))[:1]
        else:
            to_keep = query.pop(kv, None)
            if to_keep:
                keep[kv] = tuple(set(to_keep))
        for v in keep[kv]:
            query_list.append((kv, v))
    return urlencode(query_list, doseq=True)


def dedup_url(url, singles=None, ignore=None, remove=None):
    singles = singles if singles else ()
    ignore = ignore if ignore else ()
    remove = remove if remove else ()
    urltuple = urlparse(url)
    deduped_query = dedup_query(urltuple.query, singles=singles, ignore=ignore, remove=remove)
    urllist = list(urltuple)
    urllist[4] = deduped_query
    return urlunparse(urllist)
