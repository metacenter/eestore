from django.contrib import admin

from .models import Plugin


class PluginAdmin(admin.ModelAdmin):
    list_display = ('plugin_type', 'plugin_name',)
    list_filter = ('plugin_type', 'plugin_name')


admin.site.register(Plugin, PluginAdmin)
