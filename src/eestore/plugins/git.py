from os import chdir
from shutil import which
from subprocess import run  # Python 3.5+

from django.conf import settings


GIT_BINARY = getattr(settings, 'EESTORE_GIT_BINARY', which('git'))


def is_git_repo(path):
    chdir(path)
    cmd = 'git status'.split()
    returncode = run(cmd).returncode
    if returncode == 0:
        return True
    return False


def clone(url, path):
    if not is_git_repo(path):
        cmd = 'git clone {} {}'.format(url, path).split()
        returncode = run(cmd).returncode
        if returncode == 0:
            return True
        return False
    return True


def pull(path):
    chdir(path)
    if is_git_repo(path):
        cmd = 'git pull'.format(path).split()
        returncode = run(cmd).returncode
        if returncode == 0:
            return True
        return False
    return False
