from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

from . import load_plugins, get_enabled_plugins


class EEStorePluginConfig(AppConfig):
    name = 'eestore.plugins'
    verbose_name = _('Plugins')
    label = 'eestore_plugins'

    def ready(self):
        plugins = get_enabled_plugins()
        load_plugins(plugins)
