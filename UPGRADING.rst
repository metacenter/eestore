UPGRADING
=========

Normally, upgrading is just a matter of getting the new code and running
``python manage.py migrate``. Any exceptions will be listed here.

0.11
----

You should not jump directly to 0.11. Get 0.10, which removes some tables that
are no longer in use, then get 0.11, which removes the now redundant code.

If you go directly to 0.11, you will have to manually remove the following
tables, in order:

1. eestore_config_entityregistry_tags
2. eestore_config_entityregistry
3. eestore_config_tag
