=========
CHANGELOG
=========

This project adheres to `Semantic Versioning
<https://semver.org/spec/v2.0.0.html>`_) as of v0.10. Prior to
v0.10, named tags per feature, named tag plus date per feature or
commit hashes were used.

Unreleased
----------

1.0.0
-----

First major release. This marks the first iteration of the basic idea the
EEStore. There are multiple endpoints, some of which can be filled with data
via pull from remotes. There is a plugin system to pull from the remotes, and
there is a UI to make the plugins refetch data from the remotes they are hooked
up to.

There is a rudimentary user system. Any logged in user who is also superuser
may use the plugin endpoints.

0.15
----

Upgrade some dependencies.

0.14
----

Roll back to using newest Django 1.11 and nothing newer, thanks to
incompatibilities with the production postgres server.

0.13
----

* Various bugfixes and small improvements
* Make it possible to refetch data with a plugin, remotely
* Set up docker-compose for development
* Add paperwork and instructions for new contributors
* Add a changelog

... and bungle the correct flow of branches, oh well...

0.12
----

* Add stubs for documentation
* Add support for bearer token authentication
* Rewrite the plugin models completely after a deep rethink
* Add the beginning of tools to remote control the plugins

0.11
----

Complete the removal of the config app.

0.10
----

First tagged release. Only tagged versions will be used for deployment to
production from now on.

Tagged versioning was introduced in order to get rid of the unused
"config"-app. This needs to be done in two releases: first upgrading to 0.10,
migrate, then upgrading to 0.11, and another migrate.

The UPGRADING.rst was added to explain the procedure, and any future tricksy
procedures if needed.
