======================
EEStore Django Project
======================

Layout:

::

    manage.py
    requirements/
    |-- django.txt
    |-- base.txt
    \-- ..
    requirements.txt
    tests/
    src/
    \-- eestore/
       |-- site/
       |   |-- __init__.py
       |   |-- settings/
       |   |   |-- base.py
       |   |   |-- dev.py
       |   |   \-- production.py-example
       |   |-- urls.py
       |   \-- wsgi.py
       |
       \-- [apps] ..


Prerequisites
=============

- python >= 3.5
- pip
- virtualenv/wrapper (optional)
  some sql databse supported by Django

As for the database, it can run on sqlite, it's better to run it on
a postgresql, version 9.3 or newer.

Installation
============

Old-school pedal to the metal.

Creating the environment
------------------------

Create a virtual python environment for the project.
If you're not using virtualenv or virtualenvwrapper you may skip this step.

For virtualenvwrapper
.....................


::

    $ mkvirtualenv -p /usr/bin/python3 eestore-env

Activate with::

    $ workon eestore-env


For virtualenv
..............

Create a virtual env in the current directory called ``eestore-env``::

    $ virtualenv -p /usr/bin/python3 eestore-env

Activate with::

    $ source eestore-env/bin/activate


Clone the code
--------------

Obtain the url to the git repository::

    $ git clone <URL_TO_GIT_RESPOSITORY> eestore

Switch to your preferred version with ``git checkout <tag>``.


Install requirements
--------------------

Development and testing
.......................

::

    $ pip install -r requirements.txt
    $ pip install -e .[dev]

Production
..........

::

    $ pip install -r requirements.txt


Configure project
-----------------

Copy the file ``./src/eestore/site/settings/production.py-example`` to
somewhere on ``$PYTHONPATH`` and rename it to for instance ``production.py``.

Then set the environment variable ``DJANGO_SETTINGS_MODULE`` to the dotted
path of the file.

If ``DJANGO_SETTINGS_MODULE`` is not set, the settings-file
``eestore.site.settings.dev`` will be used. It has sane defaults for
development.

Setup and sync database
-----------------------

::

    $ python manage.py migrate

Running in development
======================

Old-school pedal to the metal
-----------------------------

::

    $ python manage.py runserver

This will use the settings file ``eestore.site.settings.dev`` unless
``DJANGO_SETTINGS_MODULE`` has been set.

Open browser to http://127.0.0.1:8000

Containerized
-------------

See the included ``docker-compose`` setup. If you need to test new plugins you
need to override ``DJANGO_SETTINGS_MODULE`` with your own settings-file that
sets ``EESTORE_PLUGINS``.

By default the server will be on port 9000.

Deploying to production
=======================

Ensure that ``DEBUG`` is ``False`` when in production!

Deploying to PaaSes or using containers
---------------------------------------

We recommend making a deployment-specific project that fetches the code (for
instance with ``curl``/``wget``, ``pip install`` or ``git clone``) and adds all
the necessary deployment-specific code, including any overrides for settings,
templates, static files etc.

::

    .
    |-- deploymentmethod
    |   |-- settings.py
    |   |-- wsgi.py
    |   |-- templates/
    |   |-- static/
    |   |-- requirements.txt
    |   .. deployment method specific files
    |
    .. deployment method specific files

Containerized
.............

A setup for ``docker-compose`` is included, tailored for development, not
production. Use the included development Dockerfile as a template.

If you need to make large changes to the settings, for instance changing the
list of plugins: create a local settings-file in your separate deployment repo
and set ``DJANGO_SETTINGS_MODULE`` to that file.

Deploying to hardware
---------------------

Get the code to where it needs to be, with a script utilizing ``rsync``, ``git
clone``, ``fabric`` or whatever. We recommend keeping the dependencies in
a ``virtualenv``, which means that the web server will need to know about the
path to the virtualenv.

If the virtualenv is installed at ``/path/to/virtualenv`` and the python
version is 3.4, the follwing path must be somehow added to the python path::

    /path/to/virtualenv/lib/python3.4/site-pacakges/

If using Apache, do not use ``mod_python``, use ``mod_wsgi`` in daemon mode.
Setting environment variables in Apache won't work so hardcode the settings in
the prodction settings file or import them from some file in the settings file.

Environment-variables known by the production settings
------------------------------------------------------

These MUST be set:

SECRET_KEY
    A random string about 50 characters long


DATABASE_URL
    as per `12factor <https://www.12factor.net/backing-services>`_, example:

    ``"postgresql://USERNAME:PASSWORD@HOSTNAME:PORT/DATABASE_NAME"``

    See `dj-database-url <https://pypi.python.org/pypi/dj-database-url>`_, and
    the ``docker-compose.yml``-file.

DJANGO_SETTINGS_MODULE
    Dotted path to settings-file on python path, examples:

    ``"production"``
    ``"heroku.settings"``
    ``"eestoreconfig.production"``

This MAY be set:

DEBUG
    Turn debugging on with 1, off with 0. Default off.
