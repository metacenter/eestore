FROM ubuntu:18.04 as build
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Create correct psycopg2 with great paranoia
RUN apt-get update && apt-get autoremove && \
    apt-get autoclean && apt-get install -y \
        python3-pip python3-setuptools python3-wheel \
        python3.7-dev python3.7 libpq-dev build-essential postgresql-client
# Force use of python 3.7
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 3
RUN update-alternatives --list python3
RUN python3 --version

RUN mkdir /code
WORKDIR /code

ADD requirements /code/requirements
RUN pip3 install --no-binary psycopg2 six gunicorn -r requirements/production.txt

# Reuse image with correct psycopg2 and python 3.7
FROM ubuntu:18.04
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /code/src
RUN mkdir /code
WORKDIR /code

COPY --from=build /usr/local/bin/ /usr/local/bin/
COPY --from=build /usr/local/lib/python3.7/dist-packages/ /usr/local/lib/python3.7/dist-packages/
COPY --from=build /usr/lib/python3/dist-packages/ /usr/lib/python3/dist-packages/

RUN apt-get update && apt-get autoremove && apt-get autoclean && \
    apt-get install -y postgresql-client python3.7
RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 3

RUN ls /code/
