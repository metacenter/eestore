==============
Administration
==============

A running server can be administered either via the commandline,
provided the database is reachable for ``psql`` from a machine you
have terminal access to, or via API.

Using the command line, it is not necessary to create any users on the
server. Using the API, you will need to both host all endpoints with
SSL/TLS as well as create at least one superuser on the server via the
command line.

Command line usage
==================

Manage users
------------

Create a superuser
..................

::

    python3 manage.py createsuperuser

Follow the prompts.

Due to the prompts, this cannot be run automatically in a container.

Create a bearer token for a user
................................

::

    python3 manage.py drf_create_token USERNAME

Connect installed plugins to the database
-----------------------------------------

::

    python3 manage.py sync_plugins

List available plugins
----------------------

::

    python3 manage.py list_plugins

This outputs a list of plugin classes in python dotted path notation,
followed by the abbreviation (the shortname) for that specific plugin
in parantheses.

Refresh from a remote source
----------------------------

With the source up and running, run::

    python3 manage.py refresh SHORTNAME

where ``shortname`` is found via the ``list_plugins`` command above.
(It is pointless to refresh a plugin with a shortname ending with
``local`` or ``eestore`` since these are not connected to a remote
source.)

This will refresh that specific plugin, in real-time. Depending on the
amount of data at the source, this might take a while.

Access the cached data directly
-------------------------------

If it is necessary to manipulate the contents of the database
directly, for instance by loading a source with data that can not be
pulled, you can either use raw SQL or Django's console.

Via psql
........

::

    python3 manage.py dbshell

This will give you a ``psql`` prompt at the sql server.

Via django console
..................

::

    python3 manage.py shell

This will open the Django console, but no EEStore-specific libraries
will be loaded.

API Usage
=========

Manage users
------------

This is currently not possible via the API.

Create a superuser
..................

This is currently not possible via the API.

Authentication
--------------

With a login-cookie
...................

POST a html-form with the fields username and password filed, to
``https://HOSTNAME/api-auth/login/``. You can do this via a browser as
well.

This should result in an authentication cookie.

With a bearer token
...................

Get a bearer token
,,,,,,,,,,,,,,,,,,

POST username/password to ``https://HOSTNAME/api-auth/token/`` via
html form or JSON. This should return something like::

    {'token': '9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b' }

Authenticate with bearer token
,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

Set the header ``Authorization: Bearer TOKEN`` where TOKEN is a string
you must have acquired previously, for instance via ``Get a bearer
token`` described before.

Connect installed plugins to the database
-----------------------------------------

With an authenticated user:

POST to ``https://HOSTNAME/api/plugin/sync/``, no contents in the POST
necessary.

List available plugins
----------------------

Visit (GET) ``https://HOSTNAME/api/plugin/``

Refresh from a remote source
----------------------------

With an authenticated user:

POST to ``https://HOSTNAME/api/plugin/SHORTNAME/refetch/``, no
contents in the POST necessary.

You can get the available shortnames by listing the installed plugins.
(It is pointless to refresh a plugin with a shortname ending with
``local`` or ``eestore`` since these are not connected to a remote
source.)

This will refresh that specific plugin, in a separate process.
Depending on the amount of data at the source, this might take
a while.

The call will return with status code ``202 Accepted`` and
a link to a progress-endpoint in the ``Location``-header. This
endpoint will reply with ``202 Accepted`` until the task is done, when
it will return ``200 Ok``. The new/changed records will be available at
``https://HOSTNAME/api/TYPE/`` after the refresh has finished running.

Access the cached data directly
-------------------------------

Not supported. This doesn't make sense for an API, since direct access
implies bypassing the API.
