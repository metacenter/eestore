.. EEStore documentation master file, created by
   sphinx-quickstart on Wed Jun  5 10:46:22 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to EEStore's documentation!
===================================

The EEStore (External Entity Store) is a standalone micro service
meant to pull data of a specific type from multiple remote sources
via public APIs. EEStore caches it, normalizes it and combines it into
a single API endpoint per type of data.

The data is meant to be used for menus, typeahead fields, and
similar, and contains very little information per record.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   administration


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
